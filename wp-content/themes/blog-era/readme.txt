=== Blog Era ===

Contributors: 96themes
Tags: translation-ready, custom-background, theme-options, custom-menu, threaded-comments, blog, education, news, custom-logo,right-sidebar, left-sidebar, custom-header, rtl-language-support
Requires at least: 5.0
Tested up to: 5.2.3
Requires PHP: 5.6
Stable tag: 2.0.1
License: GNU General Public License v2 or later
License URI: LICENSE

== Description ==

Blog Era is a  Responsive Blogger Template, it is clean and compatible with many devices, It’s perfect for creating your  blog, no need to coding as it is very customizable.

Blog Era WordPress Theme, Copyright (C) 2018, 96 Themes
Blog Era is distributed under the terms of the GNU GPL


== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Blog Era includes support for Infinite Scroll in Jetpack.

----------------------------------------------------------------------------------------

The exceptions to license are as follows:
Fonts
	FontAwesome 4.4.0
	Copyright 2012 Dave Gandy
	Font License: SIL OFL 1.1
	Code License: MIT License
	http://fontawesome.io/license/      

-------------------------------------------------------------------------------------------

JS Files    
	navigation JS: MIT Licenses
	https://gist.github.com/joshmcrty/926f522766a491042be1

	owl carousel Js:  MIT Licenses
	https://github.com/OwlFonk/OwlCarousel

	Skip link focus fix JS: MIT Licenses
	https://github.com/cedaro/skip-link-focus

	Jquery Meanmenu Js: MIT Licenses
	https://github.com/meanthemes/meanMenu

	ResizeSensor Js: MIT Licenses
	https://github.com/procurios/ResizeSensor

	Theia Sticky Sidebar Js: MIT Licenses
	https://github.com/WeCodePixels/theia-sticky-sidebar



---------------------------------------------------------------------------------

Images
	* https://pixabay.com/en/lake-trees-sun-foliage-mountain-2916373/
	* https://pixabay.com/en/norway-mountain-sky-blue-water-772991/
	* https://pixabay.com/en/landscape-japan-mountain-2124113/
	* https://pixabay.com/en/woman-women-office-work-business-2773007/

Other Images: 
	screenshot.png self created GPLv2
	leftsidebar.png self created GPLv2 
	no-sidebar.png self created GPLv2 
	right-sidebar.png self created GPLv2
	search-icon.png  self created GPLv2 

---------------------------------------------------------------------------------

TGM Plugin Activation  
    https://github.com/TGMPA/TGM-Plugin-Activation    
--------------------------------------------------------------------------------- 



== Changelog ==
= 2.0.1 - 07 Sep 2019 =
* Tested upto 5.2.3
* Fixed Font Issue in Elementor


= 2.0.0 - 15 July 2019 =
* Tested upto 5.2.2
* Added Compatibility to Page Builder

= 1.0.9 - 26 Jan 2019 =
* Tested upto 5.0.3.
* Modify readme file according to new format.

= 1.0.8 - 17 July 2018 =
* Tested upto 4.9.7
* Added one click demo import

= 1.0.7 - 20 Jan 2018 =
* Tested upto 4.9.2

= 1.0.6 - 26 Dec 2017 =
* Added option for scroll to top


= 1.0.5 - 11 Dec 2017 =
* Rtl Language Supported

= 1.0.4 - 30 Nov 2017 =
* Tested upto 4.9.1

= 1.0.3 - 27 Nov 2017 =
* Fix design issue.

= 1.0.2 - 26 Nov 2017 =
* Added License URI 
* Added valid Theme uri
* Fix sanitizing issue.
* Tested upto 4.9



= 1.0.1 - 12 Nov 2017 =
* Added Screentshot
* Fix Escaping and Translation.
* Remove unwanted file.
* Minor Css changes.


= 1.0.0 - October 1 2017 =
* Initial release

