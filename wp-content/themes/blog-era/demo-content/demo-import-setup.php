<?php
/**
 * Functions to provide support for the One Click Demo Import plugin (wordpress.org/plugins/one-click-demo-import)
 *
 * @package Blog_Era
 */
/**
* Remove branding
*/
add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );

/*Import demo data*/
if ( ! function_exists( 'blog_era_demo_import_files' ) ) :
    function blog_era_demo_import_files() {
        return array(
            array(
                'import_file_name'             => 'Blog Era',
                'local_import_file'            => trailingslashit( get_template_directory() ) . 'demo-content/blogera.wordpress.2018-07-17.xml',
                'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'demo-content/demo.96themes.com-blog-era-widgets.wie',
                'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'demo-content/blog-era-export.dat',
                'import_notice'                => esc_html__( 'Please waiting for a few minutes, do not close the window or refresh the page until the data is imported', 'blog-era' ),
            ),         
        );  
    }
    add_filter( 'pt-ocdi/import_files', 'blog_era_demo_import_files' );
endif;

/**
 * Action that happen after import
 */
if ( ! function_exists( 'blog_era_after_demo_import' ) ) :
function blog_era_after_demo_import( $selected_import ) {
    
        //Set Menu
        $primary_menu = get_term_by('name', 'Main Menu', 'nav_menu'); 
        $social_menu = get_term_by('name', 'Social Menu', 'nav_menu');
        $top_menu =     get_term_by('name', 'Top Menu', 'nav_menu');  

        set_theme_mod( 'nav_menu_locations' , array( 
            'menu-1' => $primary_menu->term_id,
            'social-menu' => $social_menu->term_id, 
            'top-menu' => $top_menu->term_id, 


             ) 
        );  
    
}
add_action( 'pt-ocdi/after_import', 'blog_era_after_demo_import' );
endif;