<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'blog_filcod' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'NEr7o6dv3,Ru1^v&@-SKf~}jCDA.5#2qO)?&GO7cpv<b=*)3p;OdzN{!w<M74v==' );
define( 'SECURE_AUTH_KEY',  '>hVI)O6XmFP8+6_$63>zBy~DG9n1[c`l+nvk<;X~mDH.{9]zS6+2^ZF3_<J@o){G' );
define( 'LOGGED_IN_KEY',    '@LUCuT;`*Bwyid&McSZ,~v7?.u^RPb0.AbVQALe-Q+{3U*CqNe4Q})P$;>WK*m#$' );
define( 'NONCE_KEY',        ')8hhiUD`erPWD>hF*76|&<tjmm1x+1S~>z%nM,TkH1X[oi^^jYuACGJk_aWGW:DI' );
define( 'AUTH_SALT',        '0Tdin,$LgxBA2<[Z`pHs)@n9fQ_,lBgT&@rz<C`l|m.Q.P|,?NU]op{AUwq5ZBsJ' );
define( 'SECURE_AUTH_SALT', 'IQrr 2f;p<FR~}i_yN5Kl~>alQn%:3AGV/:sXtuD[u{>[=f#pt2Cqis% kZeA)z&' );
define( 'LOGGED_IN_SALT',   '`~ZKE5{9{~<n$)@zWjSO/>R;M-m5qE0uE%y@{zgBXP%m]?[sW?{Y7<IT5-;_4[YW' );
define( 'NONCE_SALT',       '4^>O uwF~S%%F$&u=T5nbbwyo-9PH}.fF$lVX/{!aT><@!V_1,cqpp4TL&O0,X?K' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
